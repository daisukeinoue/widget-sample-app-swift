//
//  WidgetSampleAppExtension.swift
//  WidgetSampleAppExtension
//
//  Created by DaisukeInoue on 2022/10/15.
//

import WidgetKit
import SwiftUI
import Intents

struct Provider: IntentTimelineProvider {
    func placeholder(in context: Context) -> SimpleEntry {
        SimpleEntry(date: Date(), configuration: ConfigurationIntent())
    }

    func getSnapshot(for configuration: ConfigurationIntent, in context: Context, completion: @escaping (SimpleEntry) -> ()) {
        let entry = SimpleEntry(date: Date(), configuration: configuration)
        completion(entry)
    }

    func getTimeline(for configuration: ConfigurationIntent, in context: Context, completion: @escaping (Timeline<Entry>) -> ()) {
        var entries: [SimpleEntry] = []

        // Generate a timeline consisting of five entries an hour apart, starting from the current date.
        let currentDate = Date()
        
        // Entryの頻度（Widget更新頻度）は最短でも15分以上の間隔を置く事が推奨されているが、動確の為、一旦40分に設定しておく。
        let futureDate = Calendar.current.date(byAdding: .minute, value: 40, to: currentDate)!
        
        // 生成するTimelineは24時間分が良いらしいが、一旦、Entry15個分だけ生成。
        for hourOffset_ in 0 ..< 15 {
            let offset = hourOffset_ * 20
            
            // currentDateに1~15時間ずつ足し算した時間を15個生成。
            let entryDate = Calendar.current.date(byAdding: .minute, value: offset, to: currentDate)!
            
            // 毎時その時の時間を表示するEntry。
            // dateに設定した時間がWidget上に表示される。
            let entry = SimpleEntry(date: entryDate, configuration: configuration)
            entries.append(entry)
        }

        // Entry達をTimeLineに設定。
        // policyに設定したfutureDateのタイミング（今回はcurrentDateの5分後）に再度Reloadが走り、getTimeline()が呼ばれる。
        let timeline = Timeline(entries: entries, policy: .after(futureDate))
        completion(timeline)
    }
}

struct SimpleEntry: TimelineEntry {
    let date: Date
    let configuration: ConfigurationIntent
}

struct WidgetSampleAppExtensionEntryView : View {
    var entry: Provider.Entry

    var body: some View {
        Text(entry.date, style: .time)
    }
}

@main
struct WidgetSampleAppExtension: Widget {
    let kind: String = "WidgetSampleAppExtension"

    var body: some WidgetConfiguration {
        IntentConfiguration(kind: kind, intent: ConfigurationIntent.self, provider: Provider()) { entry in
            WidgetSampleAppExtensionEntryView(entry: entry)
        }
        .configurationDisplayName("My Widget")
        .description("This is an example widget.")
    }
}

struct WidgetSampleAppExtension_Previews: PreviewProvider {
    static var previews: some View {
        WidgetSampleAppExtensionEntryView(entry: SimpleEntry(date: Date(), configuration: ConfigurationIntent()))
            .previewContext(WidgetPreviewContext(family: .systemSmall))
    }
}
