//
//  WidgetSampleAppApp.swift
//  WidgetSampleApp
//
//  Created by DaisukeInoue on 2022/10/15.
//

import SwiftUI

@main
struct WidgetSampleAppApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
