//
//  ContentView.swift
//  WidgetSampleApp
//
//  Created by DaisukeInoue on 2022/10/15.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, world!")
            .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
